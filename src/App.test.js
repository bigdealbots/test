import { render, screen } from '@testing-library/react';
import { fireEvent } from '@testing-library/react'
import App from './App';

beforeEach(() => {
  // setup a DOM element as a render target
  render(<App />);
});

test("test that nice Doc Type filter is available", () => {

  // check if nice doc type filter is available
  expect(screen.getByRole("typeSearch")).toBeInTheDocument();

  // fire click event and check if right option is selected
  fireEvent.change(screen.getByTestId("typetest"), {target: {value: "Advice"}});
  let options  = screen.getAllByTestId("typeOption");
  expect(options[1].selected).toBeFalsy();
  expect(options[2].selected).toBeTruthy();
  expect(options[3].selected).toBeFalsy();
  expect(screen.getByDisplayValue("Advice")).toBeInTheDocument();

  // check if the right filter results show up
  expect(screen.getByText(/Minimal change disease and focal segmental glomerulosclerosis in adults: rituximab/i)).toBeInTheDocument();
  expect(screen.getByText(/Hyperhidrosis: oxybutynin/i)).toBeInTheDocument();

});


test("test that niceAdviceType filter is available", () => {

  // check if nice advice type filter is available
  expect(screen.getByRole("adviceprogrammeSearch")).toBeInTheDocument();

  // fire click event and check if right option is selected
  fireEvent.change(screen.getByTestId("adviceprogrammetest"), {target: {value: "Evidence summaries"}});
  let options  = screen.getAllByTestId("adviceprogrammeOption");
  expect(options[1].selected).toBeFalsy();
  expect(options[2].selected).toBeTruthy();
  expect(options[3].selected).toBeFalsy();
  expect(screen.getByDisplayValue("Evidence summaries")).toBeInTheDocument();

  // check if the right filter results show up
  expect(screen.getByText(/Mitochondrial disorders in children: Co-enzyme Q10/i)).toBeInTheDocument();
  expect(screen.getByText(/Non-cystic fibrosis bronchiectasis: inhaled tobramycin/i)).toBeInTheDocument();
});

test("test that niceGuidanceType filter is available", () => {

  // check if nice guidance type filter is available
  expect(screen.getByRole("guidanceprogrammeSearch")).toBeInTheDocument();
  
  // fire click event and check if right option is selected
  fireEvent.change(screen.getByTestId("guidanceprogrammetest"), {target: {value: "NICE guidelines"}});
  let options  = screen.getAllByTestId("guidanceprogrammeOption");
  expect(options[1].selected).toBeFalsy();
  expect(options[3].selected).toBeTruthy();
  expect(options[4].selected).toBeFalsy();
  expect(screen.getByDisplayValue("NICE guidelines")).toBeInTheDocument();

  // check if the right filter results show up
  expect(screen.getByText(/Metastatic malignant disease of unknown primary origin in adults: diagnosis and management/i)).toBeInTheDocument();
  expect(screen.getByText(/Delirium: prevention, diagnosis and management/i)).toBeInTheDocument();

});



