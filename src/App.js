import { useEffect, useState } from "react";
import 'materialize-css';
import "./App.css";
import Data from "./data.json";


const App = (props)  => {
  const [dataset, setDataset] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchParams, setSearchParams]= useState({
    niceDocType: "",
    niceAdviceType: "",
    niceGuidanceType: ""
  });
  const itemsPerPage = 10;
  const lastItemIndex = currentPage * itemsPerPage;
  const firstItemIndex = lastItemIndex - itemsPerPage;
  const currentDocuments = dataset.slice(firstItemIndex, lastItemIndex);
 
  const niceDocTypes = Data && Data.navigators && Array.isArray(Data.navigators) ? Data.navigators.map((item) => { return {title: item.displayName, items: item.modifiers.map((result) => result.displayName)}}) : null;
  

  // To display page numbers
  const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(dataset.length / itemsPerPage); i++) {
      pageNumbers.push(i);
  }

  const filterType = {
   "type": "niceDocType",
   "adviceprogramme": "niceAdviceType",
   "guidanceprogramme": "niceGuidanceType"
  }
  
  const handleSelect = (event) => {
     event.preventDefault();
     setSearchParams({
       ...searchParams,
       [filterType[event.target.name]]: event.target.value
      })   
     setDataset(Data.documents);
  }
  

  useEffect(()=>{
    
     if(Object.values(searchParams).every(item => item === null || item === "")){
        setDataset(Data.documents);
     }
     else {
     
      let validSearchParams = Object.keys(searchParams).filter(item => searchParams[item] !== "");
      
      const filtered = dataset.filter((item => {
        
        for (let key of validSearchParams){
          if(item[key] === ""){
            return false
          }
          else {
            let foundData = validSearchParams.map((param) => (item[param].includes(searchParams[param])))
            let result = foundData.every(Boolean);
            return result
          }
        }
    
      }   
      ));
      setDataset(filtered);
    
     }
     
    
  }, [searchParams])

  const handleClick = (e) => {
    e.preventDefault();
    setCurrentPage( Number(e.target.id))
    const active = document.querySelector('.active');
    if(active){
      active.classList.remove('active');
    }
    e.target.parentNode.className += "active";
   
  }
  
  const renderPageNumbers = pageNumbers.map(number => {
    return (
      <li key={number} className={number === 1 ? 'active': null}>
        <a href="#" id={number}  onClick={handleClick}>{number}</a>
      </li>
    );
  });
   
  const filters = niceDocTypes && Array.isArray(niceDocTypes) && niceDocTypes.length > 0 ? 
       
        niceDocTypes.map((searchParams, index) => {
          let filterLabel = searchParams.title.replace(/\s/g, "").toLowerCase();
          return (
            <div key={index} role={filterLabel+ "Search"} className="input-field col s12 m4" >
          
            <select name={filterLabel} id={filterLabel} aria-labelledby={filterLabel+ "Label"} data-testid={filterLabel + "test"} aria-describedby={filterLabel +"Desc"} onChange={handleSelect} className="browser-default">
            <option value={""} data-testid={filterLabel+"Option"} defaultValue="">--{searchParams.title}--</option>
              {searchParams.items ? searchParams.items.map((item, index) => (
                  <option value={item} key={index} data-testid={filterLabel+"Option"}>{item}</option>
              )): null}
            </select>
            </div>
           ) }
          )
        : null;
  
  
  return (
    <div className="App">

      <nav>
          <div className="nav-wrapper">
            <a href="#" className="brand-logo center">NICE Database Search</a>
          </div>
      </nav>
      
      <section>
          <div className="row filterBox">
            <div className="center" style={{fontWeight: "bold", fontSize:"16px"}}>Filter by:</div>
            {filters}
          </div>
      </section>

      {dataset.length > itemsPerPage ? <ul className="pagination">
        Pagination:  
        {renderPageNumbers}
      </ul>: null}


      <section>
        
        <div className="row">
            
          {dataset && Array.isArray(dataset) ? 
      
            currentDocuments.length ? currentDocuments.map((item, index) => (
              <div className="col s12 m6" key={index}>
                <div className="card documentCard darken-1">
                  <div className="card-content">
                    <span className="card-title itemTitle">{item.title}</span>
                    <p>Last Updated at: {item.lastUpdated} </p>
                    <p>Nice Document Type: {item.niceDocType.length ===0 ? "Not available": item.niceDocType.join(' ')}</p>
                    <p>Nice Advice Type: {item.niceAdviceType.length === 0 ? "Not available": item.niceAdviceType.join(' ')}</p>
                    <p>Nice Guidance Type: {item.niceGuidanceType.length === 0 ? "Not available": item.niceGuidanceType.join(', ')}</p>  
                  </div>
                </div>
              </div>
            )) : <div style={{fontWeight: "bold", fontSize:"16px", textAlign: "center"}}>No records found!!!</div>

          : <div>No data available</div> }
              
        </div>   
      </section>

    </div>
  );
}

export default App;
